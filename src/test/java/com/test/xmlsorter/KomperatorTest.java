package com.test.xmlsorter;
import com.test.xmlsorter.data.Printer;
import org.junit.Test;

import static org.junit.Assert.*;

public class KomperatorTest {

    private Komperator comparator = new Komperator();

    @Test
    public void compare_twoSamePrinters_0returned() {
        Printer printer = new Printer("Test", "test", "mono");
        assertEquals(0, comparator.compare(printer, printer));
    }


    @Test
    public void compare_1stStartingWithA_2ndStartingWithC_negativeReturned() {
        Printer printer1 = new Printer("Alor", "test", "mono");
        Printer printer2 = new Printer("Crown", "test", "mono");
        assertTrue(comparator.compare(printer1,printer2) < 0);
    }



    @Test
    public void compare_1stStartingWithH_2ndStartingWithD_positiveReturned() {
        Printer printer1 = new Printer("Huge", "test", "mono");
        Printer printer2 = new Printer("Drown", "test", "mono");
        assertTrue(comparator.compare(printer1,printer2) > 0);
    }
}