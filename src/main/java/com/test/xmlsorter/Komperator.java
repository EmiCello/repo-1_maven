package com.test.xmlsorter;
import com.test.xmlsorter.data.Printer;
import java.util.Comparator;

public class Komperator implements Comparator<Printer> {

    public int compare(Printer printer1, Printer printer2) {
        int result = printer2.getName().compareToIgnoreCase(printer1.getName());
        if(result != 0){
            return result;
        }else{
            return printer2.getType().compareToIgnoreCase(printer1.getType());
        }
    }


}
