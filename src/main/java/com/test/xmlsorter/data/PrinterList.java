package com.test.xmlsorter.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="supportedPrinters")
public class PrinterList{

    private List<Printer> printerList = new ArrayList<Printer>();

    public void add(Printer printer) {
        this.printerList.add(printer);
    }


    public List<Printer> getPrinterList() {
        return printerList;
    }
    @XmlElement(name="printerModel")
    public void setPrinterList(List<Printer> printerList) {
        this.printerList = printerList;
    }


}
