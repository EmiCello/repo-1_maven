package com.test.xmlsorter.data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="printerModel")
public class Printer {

    private String name;
    private  String type;
    private String color;


    public Printer(){

    }
    public Printer(String name, String type, String color){
        this.name = name;
        this.type = type;
        this.color = color;
    }

    @XmlAttribute(name="manufacturer")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @XmlAttribute(name="model")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @XmlElement(name="color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "com.test.xmlsorter.data.Printer{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' + "color='" + color + '\'' +
                '}';
    }
}
