package com.test.xmlsorter;

import com.test.xmlsorter.data.Printer;
import com.test.xmlsorter.data.PrinterList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class p1 {

    public static void main(String[] args)  {
        List<String> list = new ArrayList<>();
        File file = new File(p1.class.getResource("supported.xml").getFile());
        try{
            FileInputStream stream = new FileInputStream(file);
            InputStreamReader readIn = new InputStreamReader(stream);
            BufferedReader read = new BufferedReader(readIn);
            String line;
            while ((line = read.readLine()) != null){
                list.add(line);
            }
        }catch(IOException e){
            System.out.println("błąd wczytywania");
        }

        for(String line : list){
            System.out.println(line);
        }

        System.out.println("----------------------------");

        try{
            JAXBContext jaxbContext = JAXBContext.newInstance(Printer.class, PrinterList.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            PrinterList pl = (PrinterList) jaxbUnmarshaller.unmarshal(file);
            System.out.println(pl.getPrinterList());
            System.out.println("---------------------");
            pl.getPrinterList().sort(new Komperator());
            System.out.println("przed sortowaniem");
            System.out.println(pl.getPrinterList());
            System.out.println("po sortowaniu");

            JAXBContext jxc = JAXBContext.newInstance(Printer.class, PrinterList.class);
            Marshaller jxm = jxc.createMarshaller();
            jxm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jxm.marshal(pl, System.out);
            jxm.marshal(pl, new File("Zapis.XML.txt"));
        }catch(JAXBException e){
            System.out.println("błąd konwersji");
            e.printStackTrace();
        }





    }

}
